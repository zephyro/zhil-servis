<!doctype html>
<html class="no-js" lang="">

    <?php include('inc/head.inc.php') ?>

    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <section class="primary">
                <div class="primary__slider owl-carousel">
                    <div class="primary_slide">
                        <div class="primary__item">
                            <div class="primary__image">
                                <img src="img/homepage__top_image.jpg" class="img_fluid">
                            </div>
                            <div class="container">
                                <div class="primary__wrap">
                                    <div class="primary__inner">
                                        <div class="primary__content">
                                            <h1>Изделия из металла и дерева</h1>
                                            <div class="primary__text">Наша компания АО "Жилсервис" предлагает Вашему вниманию только качественные и профессионально сделанные изделия из метала и дерева.</div>
                                            <ul class="btn_group">
                                                <li>
                                                    <a href="#" class="btn btn_red btn_md btn_shadow">каталог</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn btn_md">консультация</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="primary_slide">
                        <div class="primary__item">
                            <div class="primary__image">
                                <img src="img/homepage__top_image.jpg" class="img_fluid">
                            </div>
                            <div class="container">
                                <div class="primary__wrap">
                                    <div class="primary__inner">
                                        <div class="primary__content">
                                            <h1>Изделия из металла и дерева</h1>
                                            <div class="primary__text">Наша компания АО "Жилсервис" предлагает Вашему вниманию только качественные и профессионально сделанные изделия из метала и дерева.</div>
                                            <ul class="btn_group">
                                                <li>
                                                    <a href="#" class="btn btn_red btn_md btn_shadow">каталог</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn btn_md">консультация</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="primary_slide">
                        <div class="primary__item">
                            <div class="primary__image">
                                <img src="img/homepage__top_image.jpg" class="img_fluid">
                            </div>
                            <div class="container">
                                <div class="primary__wrap">
                                    <div class="primary__inner">
                                        <div class="primary__content">
                                            <h1>Изделия из металла и дерева</h1>
                                            <div class="primary__text">Наша компания АО "Жилсервис" предлагает Вашему вниманию только качественные и профессионально сделанные изделия из метала и дерева.</div>
                                            <ul class="btn_group">
                                                <li>
                                                    <a href="#" class="btn btn_red btn_md btn_shadow">каталог</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn btn_md">консультация</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="primary_slide">
                        <div class="primary__item">
                            <div class="primary__image">
                                <img src="img/homepage__top_image.jpg" class="img_fluid">
                            </div>
                            <div class="container">
                                <div class="primary__wrap">
                                    <div class="primary__inner">
                                        <div class="primary__content">
                                            <h1>Изделия из металла и дерева</h1>
                                            <div class="primary__text">Наша компания АО "Жилсервис" предлагает Вашему вниманию только качественные и профессионально сделанные изделия из метала и дерева.</div>
                                            <ul class="btn_group">
                                                <li>
                                                    <a href="#" class="btn btn_red btn_md btn_shadow">каталог</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn btn_md">консультация</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="advantage">
                <div class="container">
                    <div class="advantage__row">
                        <div class="advantage__item">
                            <div class="advantage__item_icon">
                                <img src="img/advantage_01.svg" class="ico_svg" alt="">
                            </div>
                            <div class="advantage__item_title">Качество</div>
                            <div class="advantage__item_text">Наши сотрудники все профессионалы с большим опытом работы, что гарантирует высокое качество продукции.</div>
                        </div>
                        <div class="advantage__item">
                            <div class="advantage__item_icon">
                                <img src="img/advantage_02.svg" class="ico_svg" alt="">
                            </div>
                            <div class="advantage__item_title">Стоимость</div>
                            <div class="advantage__item_text">У нас собственное производство и прямые контакты с поставщиками. Это гарантирует приятную для Вас цену.</div>
                        </div>
                        <div class="advantage__item">
                            <div class="advantage__item_icon">
                                <img src="img/advantage_03.svg" class="ico_svg" alt="">
                            </div>
                            <div class="advantage__item_title">Доставка</div>
                            <div class="advantage__item_text">Доставка происходит любым для Вас удобным способом по России и странам СНГ прямо к Вашему порогу.</div>
                        </div>
                        <div class="advantage__item">
                            <div class="advantage__item_icon">
                                <img src="img/advantage_04.svg" class="ico_svg" alt="">
                            </div>
                            <div class="advantage__item_title">Акции</div>
                            <div class="advantage__item_text">У нас всегда присутствуют приятные скидки для постоянных клиентов, а также акции для новых покупателей.</div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="cat">
                <div class="container">
                    <h2>каталог продукции</h2>
                    <div class="cat__row">
                        <div class="cat__item">
                            <a href="#" class="cat__item_image">
                                <img src="images/cat_01.jpg" class="img_fluid" alt="">
                            </a>
                            <div class="cat__item_name"><span>Малые архитектурные формы</span></div>
                            <div class="text_center">
                                <a href="#" class="btn btn_border_red">перейти в каталог</a>
                            </div>
                        </div>
                        <div class="cat__item">
                            <a href="#" class="cat__item_image">
                                <img src="images/cat_02.jpg" class="img_fluid" alt="">
                            </a>
                            <div class="cat__item_name"><span>Изделия из металла и дерева</span></div>
                            <div class="text_center">
                                <a href="#" class="btn btn_border_red">перейти в каталог</a>
                            </div>
                        </div>
                        <div class="cat__item">
                            <a href="#" class="cat__item_image">
                                <img src="images/cat_03.jpg" class="img_fluid" alt="">
                            </a>
                            <div class="cat__item_name"><span>Изделия с элементами художественной ковки</span></div>
                            <div class="text_center">
                                <a href="#" class="btn btn_border_red">перейти в каталог</a>
                            </div>
                        </div>
                        <div class="cat__item">
                            <a href="#" class="cat__item_image">
                                <img src="images/cat_04.jpg" class="img_fluid" alt="">
                            </a>
                            <div class="cat__item_name"><span>Изделия из листового металла с фигурными элементами и без</span></div>
                            <div class="text_center">
                                <a href="#" class="btn btn_border_red">перейти в каталог</a>
                            </div>
                        </div>
                        <div class="cat__item">
                            <a href="#" class="cat__item_image">
                                <img src="images/cat_05.jpg" class="img_fluid" alt="">
                            </a>
                            <div class="cat__item_name"><span>Входные группы для жилого фонда и технических помещений</span></div>
                            <div class="text_center">
                                <a href="#" class="btn btn_border_red">перейти в каталог</a>
                            </div>
                        </div>
                        <div class="cat__item">
                            <a href="#" class="cat__item_image">
                                <img src="images/cat_06.jpg" class="img_fluid" alt="">
                            </a>
                            <div class="cat__item_name"><span>Изделия для отдыха и работы</span></div>
                            <div class="text_center">
                                <a href="#" class="btn btn_border_red">перейти в каталог</a>
                            </div>
                        </div>
                        <div class="cat__item">
                            <a href="#" class="cat__item_image">
                                <img src="images/cat_07.jpg" class="img_fluid" alt="">
                            </a>
                            <div class="cat__item_name"><span> Технические и бытовые емкости</span></div>
                            <div class="text_center">
                                <a href="#" class="btn btn_border_red">перейти в каталог</a>
                            </div>
                        </div>
                        <div class="cat__item">
                            <a href="#" class="cat__item_image">
                                <img src="images/cat_08.jpg" class="img_fluid" alt="">
                            </a>
                            <div class="cat__item_name"><span>Бетонные изделия</span></div>
                            <div class="text_center">
                                <a href="#" class="btn btn_border_red">перейти в каталог</a>
                            </div>
                        </div>
                        <div class="cat__item">
                            <a href="#" class="cat__item_image">
                                <img src="images/cat_09.jpg" class="img_fluid" alt="">
                            </a>
                            <div class="cat__item_name"><span>Столярные изделия</span></div>
                            <div class="text_center">
                                <a href="#" class="btn btn_border_red">перейти в каталог</a>
                            </div>
                        </div>
                        <div class="cat__item">
                            <a href="#" class="cat__item_image">
                                <img src="images/cat_10.jpg" class="img_fluid" alt="">
                            </a>
                            <div class="cat__item_name"><span>ПВХ окна и двери</span></div>
                            <div class="text_center">
                                <a href="#" class="btn btn_border_red">перейти в каталог</a>
                            </div>
                        </div>
                        <div class="cat__item">
                            <a href="#" class="cat__item_image">
                                <img src="images/cat_11.jpg" class="img_fluid" alt="">
                            </a>
                            <div class="cat__item_name"><span>Порошковая покраска</span></div>
                            <div class="text_center">
                                <a href="#" class="btn btn_border_red">перейти в каталог</a>
                            </div>
                        </div>
                        <div class="cat__item">
                            <a href="#" class="cat__item_image">
                                <img src="images/cat_12.jpg" class="img_fluid" alt="">
                            </a>
                            <div class="cat__item_name"><span>Оборудование для детских игровых площадок, тренажеров и комплексов</span></div>
                            <div class="text_center">
                                <a href="#" class="btn btn_border_red">перейти в каталог</a>
                            </div>
                        </div>
                        <div class="cat__item">
                            <a href="#" class="cat__item_image">
                                <img src="images/cat_13.jpg" class="img_fluid" alt="">
                            </a>
                            <div class="cat__item_name"><span>Садовая мебель и дизайнерские решения для интерьеров</span></div>
                            <div class="text_center">
                                <a href="#" class="btn btn_border_red">перейти в каталог</a>
                            </div>
                        </div>
                        <div class="cat__item">
                            <a href="#" class="cat__item_image">
                                <img src="images/cat_14.jpg" class="img_fluid" alt="">
                            </a>
                            <div class="cat__item_name"><span>Любые креативные пожелания заказчика</span></div>
                            <div class="text_center">
                                <a href="#" class="btn btn_border_red">перейти в каталог</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <div class="question">
                <div class="container">
                    <?php include('inc/question.inc.php') ?>
                </div>
            </div>

            <section class="why">
                <div class="container">
                    <h2>почему мы?</h2>
                    <div class="why__row">
                        <div class="why__item">
                            <div class="why__item_icon">
                                <img src="img/why_01.svg" alt="" class="ico_svg">
                            </div>
                            <div class="why__item_name">Свое производство</div>
                            <div class="why__item_text">Наличие своего производственного комплекса полного цикла поставки сырья до выхода готовой продукции.</div>
                        </div>
                        <div class="why__item">
                            <div class="why__item_icon">
                                <img src="img/why_02.svg" alt="" class="ico_svg">
                            </div>
                            <div class="why__item_name">Качественное оборудование</div>
                            <div class="why__item_text">Профессиональное оборудование позволяет делать нам очень точную и качественную продукцию.</div>
                        </div>
                        <div class="why__item">
                            <div class="why__item_icon">
                                <img src="img/why_03.svg" alt="" class="ico_svg">
                            </div>
                            <div class="why__item_name">Логистический центр</div>
                            <div class="why__item_text">Планирование и распределение товарного потока в точные сроки по всей России и   странам СНГ.</div>
                        </div>
                        <div class="why__item">
                            <div class="why__item_icon">
                                <img src="img/why_04.svg" alt="" class="ico_svg">
                            </div>
                            <div class="why__item_name">Складские запасы товара</div>
                            <div class="why__item_text">У нас всегда все есть в наличии, клиенту не нужно долго ждать изделие, кроме индивидуальных проектов.</div>
                        </div>
                        <div class="why__item">
                            <div class="why__item_icon">
                                <img src="img/why_05.svg" alt="" class="ico_svg">
                            </div>
                            <div class="why__item_name">Отдел дизайна</div>
                            <div class="why__item_text">Наличие квалифицированного персонала в области проектов и разработки дизайна, можем воплотить любой Ваш проект в мечту.</div>
                        </div>
                        <div class="why__item">
                            <div class="why__item_icon">
                                <img src="img/why_06.svg" alt="" class="ico_svg">
                            </div>
                            <div class="why__item_name">Брендирование изделий</div>
                            <div class="why__item_text">Наши изделия имеют свой бренд "Жилксервис", мы гордимся нашим качеством и остерегаем Вас от копий.</div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="partners">
                <div class="container">
                    <h2>Наши клиенты и партнеры</h2>
                    <div class="partners__row">
                        <div class="partners__item">
                            <img src="img/partners/p_logo_01.png" class="img_fluid" alt="">
                        </div>
                        <div class="partners__item">
                            <img src="img/partners/p_logo_02.png" class="img_fluid" alt="">
                        </div>
                        <div class="partners__item">
                            <img src="img/partners/p_logo_03.png" class="img_fluid" alt="">
                        </div>
                        <div class="partners__item">
                            <img src="img/partners/p_logo_04.png" class="img_fluid" alt="">
                        </div>
                        <div class="partners__item">
                            <img src="img/partners/p_logo_05.png" class="img_fluid" alt="">
                        </div>
                        <div class="partners__item">
                            <img src="img/partners/p_logo_06.png" class="img_fluid" alt="">
                        </div>
                        <div class="partners__item">
                            <img src="img/partners/p_logo_07.png" class="img_fluid" alt="">
                        </div>
                        <div class="partners__item">
                            <img src="img/partners/p_logo_08.png" class="img_fluid" alt="">
                        </div>
                        <div class="partners__item">
                            <img src="img/partners/p_logo_09.png" class="img_fluid" alt="">
                        </div>
                        <div class="partners__item">
                            <img src="img/partners/p_logo_10.png" class="img_fluid" alt="">
                        </div>
                    </div>
                </div>
            </section>

            <section class="about">
                <div class="container">
                    <h2>о компании АО "ЖИЛСЕРВИС</h2>
                    <div class="about__row">
                        <div class="about__video">
                            <a href="https://youtu.be/i5tJ5LE-Iq0" data-fancybox>
                                <img src="img/about__video.jpg" class="img_fluid" alt="">
                            </a>
                        </div>
                        <div class="about__text">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vel vestibulum dolor. Donec ut varius dui. Donec laoreet, nisl convallis ornare accumsan, diam velit porta libero, vel elementum mi risus vel purus. Donec tempor rutrum sollicitudin. Ut condimentum urna a massa scelerisque, nec gravida sapien rutrum. Aenean nec pellentesque massa. Aliquam dui felis, pretium at malesuada ac, pulvinar eu quam. Duis in congue diam, non egestas felis. Maecenas nec est a sem iaculis placerat. Suspendisse suscipit efficitur nulla, a fringilla arcu scelerisque sit amet. Vestibulum maximus augue vel nunc mattis, ut sodales nisi dapibus. Duis sit amet mauris sed purus bibendum efficitur. Morbi gravida condimentum diam, eget iaculis ipsum.</p>
                        </div>
                    </div>
                </div>
            </section>

            <?php include('inc/contacts.inc.php') ?>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/modal.inc.php') ?>

        <?php include('inc/scripts.inc.php') ?>

    </body>
</html>

<!doctype html>
<html class="no-js" lang="">

    <?php include('inc/head.inc.php') ?>

    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <section class="heading">
                <div class="heading__image">
                    <img src="img/heading__image.jpg" class="img_fluid">
                </div>
                <div class="container">
                    <div class="heading__wrap">
                        <div class="heading__inner">
                            <div class="heading__content">
                                <h1>Single page</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="main">
                <div class="container">

                </div>
            </section>

            <?php include('inc/contacts.inc.php') ?>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

    </body>
</html>

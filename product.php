<!doctype html>
<html class="no-js" lang="">

    <?php include('inc/head.inc.php') ?>

    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <section class="heading">
                <div class="heading__image">
                    <img src="img/heading__image.jpg" class="img_fluid">
                </div>
                <div class="container">
                    <div class="heading__wrap">
                        <div class="heading__inner">
                            <div class="heading__content">
                                <h1>Детская песочница со счетами</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="main">
                <div class="container">
                    <div class="product">
                        <div class="product__gallery">
                            <div class="product__gallery_image">
                                <div class="gallery_slider owl-carousel">
                                    <div data-hash="zero">
                                        <img src="images/product.jpg" class="img_fluid" alt="">
                                    </div>
                                    <div data-hash="one">
                                        <img src="images/product.jpg" class="img_fluid" alt="">
                                    </div>
                                    <div data-hash="two">
                                        <img src="images/product.jpg" class="img_fluid" alt="">
                                    </div>
                                </div>

                            </div>
                            <div class="product__gallery_thumbs">
                                <a href="#zero" class="product__gallery_thumb  ">
                                    <img src="images/product__thumb_01.jpg" class="img_fluid" alt="">
                                </a>
                                <a href="#one" class="product__gallery_thumb  ">
                                    <img src="images/product__thumb_02.jpg" class="img_fluid" alt="">
                                </a>
                                <a href="#two" class="product__gallery_thumb  ">
                                    <img src="images/product__thumb_03.jpg" class="img_fluid" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="product__info">
                            <div class="product__info_title">Описание товара</div>
                            <div class="product__info_text">Практически ни одна детская площадка не обходится без песочницы, которая занимает детишек абсолютно разного возраста. Ребята помладше играют с формочками и ведёрками, дети постарше возводят целые песчаные замки.</div>
                            <div class="product__info_price"><span>Стоимость: </span> <strong>18000р</strong></div>
                            <a href="#order" class="btn btn_red btn_shadow btn_order">купить</a>
                        </div>
                    </div>
                </div>
            </section>

            <div class="like">
                <div class="container">
                    <h2>Похожие товары</h2>
                    <div class="goods_wrap">
                        <div class="row">
                            <div class="col col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 col-gutter-lr">
                                <div class="goods">
                                    <a class="goods__image">
                                        <img src="images/goods_01.jpg" class="img_fluid" alt="">
                                    </a>
                                    <div class="goods__title"><span>Детская песочница со счетами</span></div>
                                    <ul class="goods__buttons">
                                        <li>
                                            <a href="#" class="btn">подробнее</a>
                                        </li>
                                        <li>
                                            <a href="#order" class="btn btn_red btn_shadow btn_order">купить</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 col-gutter-lr">
                                <div class="goods">
                                    <a class="goods__image">
                                        <img src="images/goods_02.jpg" class="img_fluid" alt="">
                                    </a>
                                    <div class="goods__title"><span>Детские качели для площадки</span></div>
                                    <ul class="goods__buttons">
                                        <li>
                                            <a href="#" class="btn">подробнее</a>
                                        </li>
                                        <li>
                                            <a href="#order" class="btn btn_red btn_shadow btn_order">купить</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 col-gutter-lr">
                                <div class="goods">
                                    <a class="goods__image">
                                        <img src="images/goods_03.jpg" class="img_fluid" alt="">
                                    </a>
                                    <div class="goods__title"><span>Детская карусель на 4 места</span></div>
                                    <ul class="goods__buttons">
                                        <li>
                                            <a href="#" class="btn">подробнее</a>
                                        </li>
                                        <li>
                                            <a href="#order" class="btn btn_red btn_shadow btn_order">купить</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 col-gutter-lr">
                                <div class="goods">
                                    <a class="goods__image">
                                        <img src="images/goods_04.jpg" class="img_fluid" alt="">
                                    </a>
                                    <div class="goods__title"><span>Детская карусель на 3 места</span></div>
                                    <ul class="goods__buttons">
                                        <li>
                                            <a href="#" class="btn">подробнее</a>
                                        </li>
                                        <li>
                                            <a href="#order" class="btn btn_red btn_shadow btn_order">купить</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php include('inc/contacts.inc.php') ?>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/modal.inc.php') ?>

        <?php include('inc/scripts.inc.php') ?>

    </body>
</html>

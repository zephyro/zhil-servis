(function() {

    $('.nav_toggle').on('click touchstart', function(e){
        e.preventDefault();
        $('.header').toggleClass('nav_open');
    });

}());

$(function($){

    var $h = $('.header').offset().top;

    $(window).scroll(function(){

        if ( $(window).scrollTop() > $h) {
            $('.header').addClass('fix_top');
        }else{
            $('.header').removeClass('fix_top');
        }
    });
});

$('.primary__slider').owlCarousel({
    loop:true,
    items:1,
    margin:0,
    nav:false
});


$(".btn_modal").fancybox({
    'padding'    : 0
});

$(".btn_order").fancybox({
    'padding'    : 0
});


$('.gallery_slider').owlCarousel({
    items:1,
    loop:false,
    center:false,
    margin:10,
    URLhashListener:true,
    autoplayHoverPause:true,
    startPosition: 'URLHash'
});

<!-- Callback -->
<div class="hide">
    <div class="modal" id="callback">
        <div class="modal__row">
            <div class="modal__content">
                <div class="modal__title">
                    <strong>закажите обратный звонок</strong>
                    <span>мы перезвоним вам в ближайшее время</span>
                </div>
                <form class="form">
                    <div class="form_group">
                        <input type="text" class="form_control" name="name" placeholder="Ваше имя">
                    </div>
                    <div class="form_group">
                        <input type="text" class="form_control" name="phone" placeholder="Ваш номер телефона">
                    </div>
                    <div class="text_center">
                        <button type="submit" class="btn btn_red btn_shadow">заказать звонок</button>
                    </div>
                </form>
            </div>
            <div class="modal__image" style="background-image: url('img/modal__bg_01.jpg')"></div>
        </div>
    </div>
</div>

<!-- Callback Success -->
<div class="hide">
    <a href="#callback_success" class="success_callback_open btn_modal"></a>
    <div class="modal" id="callback_success">
        <div class="modal__row">
            <div class="modal__success">
                <div class="modal__success_wrap">
                    <div class="modal__success_icon">
                        <img src="img/icon__success.svg" class="img_fluid" alt="">
                    </div>
                    <div class="modal__title">
                        <strong>ваша заявка принята</strong>
                        <span>наш менеджер перезвонит вам в ближайшее время</span>
                    </div>
                </div>
            </div>
            <div class="modal__image" style="background-image: url('img/modal__bg_01.jpg')"></div>
        </div>
    </div>
</div>

<!-- Order -->
<div class="hide">
    <a href="#order" class="order_open btn_modal"></a>
    <div class="modal" id="order">
        <div class="modal__row">
            <div class="modal__content">
                <div class="modal__title">
                    <strong>Детская песочница со счетами</strong>
                </div>
                <form class="form">
                    <div class="form_group">
                        <input type="text" class="form_control" name="name" placeholder="Ваше имя">
                    </div>
                    <div class="form_group">
                        <input type="text" class="form_control" name="phone" placeholder="Ваш номер телефона">
                    </div>
                    <div class="form_group">
                        <input type="text" class="form_control" name="email" placeholder="Ваш email">
                    </div>
                    <div class="text_center">
                        <button type="submit" class="btn btn_red btn_shadow">заказать звонок</button>
                    </div>
                </form>
            </div>
            <div class="modal__image" style="background-image: url('img/modal__bg_02.jpg')"></div>
        </div>
    </div>
</div>

<!-- Order Success -->
<div class="hide">
    <a href="#order_success" class="success_order_open btn_modal"></a>
    <div class="modal" id="order_success">
        <div class="modal__row">
            <div class="modal__success">
                <div class="modal__success_wrap">
                    <div class="modal__success_icon">
                        <img src="img/icon__success.svg" class="img_fluid" alt="">
                    </div>
                    <div class="modal__title">
                        <strong>ваша заявка принята</strong>
                        <span>наш менеджер перезвонит вам в ближайшее время</span>
                    </div>
                </div>
            </div>
            <div class="modal__image" style="background-image: url('img/modal__bg_02.jpg')"></div>
        </div>
    </div>
</div>
<footer class="footer">
    <div class="container">
        <div class="footer__row">
            <div class="footer__content">
                <a href="/" class="footer__logo">
                    <img src="img/logo.svg" class="img_fluid" alt="">
                </a>
                <div class="footer__text">Copyright © www.gilservis.ru Все права защищены</div>
            </div>
            <div class="footer__contact">
                <a href="tel:8 800 960 66 66" class="footer__phone">
                    <i>
                        <img src="img/icon__phone.svg" class="img_fluid" alt="">
                    </i>
                    <span>Есть вопросы?</span>
                    <strong>8 800 960 66 66</strong>
                </a>
                <a href="#callback" class="btn btn_red btn_shadow btn_modal">заказать звонок</a>
            </div
        </div>
    </div>
</footer>
<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
<script src="js/vendor/jquery.fancybox/jquery.fancybox.min.js"></script>
<script src="js/vendor/OwlCarousel2/owl.carousel.min.js"></script>

<script src="js/plugins.js"></script>
<script src="js/main.js"></script>


<!--- Map -->
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script>

    ymaps.ready(init);

    function init() {
        var myMap = new ymaps.Map("map", {
            center: [56.72714606799689,37.143091499999954],
            zoom: 14,
            controls: ['smallMapDefaultSet']
        }, {

        });

        myMap.geoObjects
            .add(new ymaps.Placemark([56.72714606799689,37.143091499999954], {
                balloonContent: '',
                iconCaption: ''
            }, {
                // Опции.
                // Необходимо указать данный тип макета.
                iconLayout: 'default#image',
                // Своё изображение иконки метки.
                iconImageHref: 'img/icon__maps_flag.png',
                // Размеры метки.
                iconImageSize: [32, 44],
                // Смещение левого верхнего угла иконки относительно
                // её "ножки" (точки привязки).
                iconImageOffset: [-16, -44]
            }));
    }
</script>
<!--- -->
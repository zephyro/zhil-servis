

<div class="question__row">
    <div class="question__content">
        <h2>У Вас есть вопросы? </h2>
        <div class="question__text">
            <p>Заполните форму справа и наш специалист свяжется с Вами в ближайшее время.</p>
            <p>Обычно это 15-30 минут в рабочее время.</p>
        </div>
    </div>
    <div class="question__form">
        <div class="question__form_wrap">
            <div class="question__form_title">заполните форму и мы ответим на ваши вопросы</div>
            <div class="form_group">
                <input type="text" class="form_control" name="name" placeholder="Ваше имя">
            </div>
            <div class="form_group">
                <input type="text" class="form_control" name="phone" placeholder="Ваш номер телефона">
            </div>
            <div class="form_group">
                <input type="text" class="form_control" name="email" placeholder="Ваш email">
            </div>
            <div class="text_center">
                <button type="submit" class="btn btn_red btn_shadow">Отправить</button>
            </div>
        </div>
    </div>
</div>
<header class="header">
    <div class="container">
        <div class="header__row">
            <a class="header__logo" href="/">
                <img src="img/logo.svg" class="img_fluid" alt="">
            </a>
            <div class="header__content">
                <ul class="header__nav">
                    <li><a href="#"><span>контакты</span></a></li>
                    <li><a href="#"><span>почему мы?</span></a></li>
                    <li><a href="#"><span>Каталог</span></a></li>
                    <li><a href="#"><span>преимущества</span></a></li>
                </ul>

                <ul class="header__contact">
                    <li>
                        <a href="tel:8 800 960 66 66" class="header__phone">
                            <i>
                                <img src="img/icon__header_phone.svg" class="img_fluid" alt="">
                            </i>
                            <span>Есть вопросы?</span>
                            <strong>8 800 960 66 66</strong>
                        </a>
                    </li>
                    <li>
                        <a href="#callback" class="btn btn_red btn_shadow btn_modal">заказать звонок</a>
                    </li>
                </ul>
            </div>

            <a href="#" class="header__toggle nav_toggle">
                <span></span>
            </a>
        </div>
    </div>
</header>
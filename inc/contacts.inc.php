<section class="contacts">
    <div class="container">
        <div class="contacts__box">
            <div class="contacts__box_wrap">
                <h2>Контакты</h2>
                <div class="contacts__item contacts__item_phone">
                    <span>Телефон:</span>
                    <strong><a href="tel:88009606666">8 800 960 66 66</a></strong>
                </div>
                <div class="contacts__item contacts__item_email">
                    <span>Email:</span>
                    <strong><a href="mailto:info@gilkomservis.ru">info@gilkomservis.ru</a></strong>
                </div>
                <div class="contacts__item contacts__item_address">
                    <span>Адрес:</span>
                    <strong>г. Казань, ул. Октябрьская, 1а</strong>
                </div>
            </div>
        </div>
    </div>
    <div class="contacts__map" id="map">

    </div>
</section>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700&display=swap&subset=cyrillic" rel="stylesheet">
    <link rel="stylesheet" href="fonts/SegoeUI/SegoeUI.css">
    <link rel="stylesheet" href="js/vendor/jquery.fancybox/jquery.fancybox.min.css">
    <link rel="stylesheet" href="js/vendor/OwlCarousel2/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="js/vendor/OwlCarousel2/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/main.css">

</head>
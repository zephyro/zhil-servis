<!doctype html>
<html class="no-js" lang="">

    <?php include('inc/head.inc.php') ?>

    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <section class="heading">
                <div class="heading__image">
                    <img src="img/heading__image.jpg" class="img_fluid">
                </div>
                <div class="container">
                    <div class="heading__wrap">
                        <div class="heading__inner">
                            <div class="heading__content">
                                <h1>Малые архитектурные формы</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="main">
                <div class="container">

                    <div class="goods_wrap">
                        <div class="row">
                            <div class="col col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 col-gutter-lr">
                                <div class="goods">
                                    <a class="goods__image">
                                        <img src="images/goods_01.jpg" class="img_fluid" alt="">
                                    </a>
                                    <div class="goods__title"><span>Детская песочница со счетами</span></div>
                                    <ul class="goods__buttons">
                                        <li>
                                            <a href="#" class="btn">подробнее</a>
                                        </li>
                                        <li>
                                            <a href="#order" class="btn btn_red btn_shadow btn_order">купить</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 col-gutter-lr">
                                <div class="goods">
                                    <a class="goods__image">
                                        <img src="images/goods_02.jpg" class="img_fluid" alt="">
                                    </a>
                                    <div class="goods__title"><span>Детские качели для площадки</span></div>
                                    <ul class="goods__buttons">
                                        <li>
                                            <a href="#" class="btn">подробнее</a>
                                        </li>
                                        <li>
                                            <a href="#order" class="btn btn_red btn_shadow btn_order">купить</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 col-gutter-lr">
                                <div class="goods">
                                    <a class="goods__image">
                                        <img src="images/goods_03.jpg" class="img_fluid" alt="">
                                    </a>
                                    <div class="goods__title"><span>Детская карусель на 4 места</span></div>
                                    <ul class="goods__buttons">
                                        <li>
                                            <a href="#" class="btn">подробнее</a>
                                        </li>
                                        <li>
                                            <a href="#order" class="btn btn_red btn_shadow btn_order">купить</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 col-gutter-lr">
                                <div class="goods">
                                    <a class="goods__image">
                                        <img src="images/goods_04.jpg" class="img_fluid" alt="">
                                    </a>
                                    <div class="goods__title"><span>Детская карусель на 3 места</span></div>
                                    <ul class="goods__buttons">
                                        <li>
                                            <a href="#" class="btn">подробнее</a>
                                        </li>
                                        <li>
                                            <a href="#order" class="btn btn_red btn_shadow btn_order">купить</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 col-gutter-lr">
                                <div class="goods">
                                    <a class="goods__image">
                                        <img src="images/goods_05.jpg" class="img_fluid" alt="">
                                    </a>
                                    <div class="goods__title"><span>Детский домик со счетами</span></div>
                                    <ul class="goods__buttons">
                                        <li>
                                            <a href="#" class="btn">подробнее</a>
                                        </li>
                                        <li>
                                            <a href="#order" class="btn btn_red btn_shadow btn_order">купить</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 col-gutter-lr">
                                <div class="goods">
                                    <a class="goods__image">
                                        <img src="images/goods_06.jpg" class="img_fluid" alt="">
                                    </a>
                                    <div class="goods__title"><span>Детская песочница домик</span></div>
                                    <ul class="goods__buttons">
                                        <li>
                                            <a href="#" class="btn">подробнее</a>
                                        </li>
                                        <li>
                                            <a href="#order" class="btn btn_red btn_shadow btn_order">купить</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php include('inc/question.inc.php') ?>

                    <div class="content pt_80">
                        <h2>Малые архитектурные формы</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vel vestibulum dolor. Donec ut varius dui. Donec laoreet, nisl convallis ornare accumsan, diam velit porta libero, vel elementum mi risus vel purus. Donec tempor rutrum sollicitudin. Ut condimentum urna a massa scelerisque, nec gravida sapien rutrum. Aenean nec pellentesque massa. Aliquam dui felis, pretium at malesuada ac, pulvinar eu quam. Duis in congue diam, non egestas felis. Maecenas nec est a sem iaculis placerat. Suspendisse suscipit efficitur nulla, a fringilla arcu scelerisque sit amet. Vestibulum maximus augue vel nunc mattis, ut sodales nisi dapibus. Duis sit amet mauris sed purus bibendum efficitur. Morbi gravida condimentum diam, eget iaculis ipsum.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vel vestibulum dolor. Donec ut varius dui. Donec laoreet, nisl convallis ornare accumsan, diam velit porta libero, vel elementum mi risus vel purus. Donec tempor rutrum sollicitudin. Ut condimentum urna a massa scelerisque, nec gravida sapien rutrum. Aenean nec pellentesque massa. Aliquam dui felis, pretium at malesuada ac, pulvinar eu quam. Duis in congue diam, non egestas felis. Maecenas nec est a sem iaculis placerat. Suspendisse suscipit efficitur nulla, a fringilla arcu scelerisque sit amet. Vestibulum maximus augue vel nunc mattis, ut sodales nisi dapibus. Duis sit amet mauris sed purus bibendum efficitur. Morbi gravida condimentum diam, eget iaculis ipsum.</p>
                    </div>

                </div>
            </section>

            <?php include('inc/contacts.inc.php') ?>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/modal.inc.php') ?>

        <?php include('inc/scripts.inc.php') ?>

    </body>
</html>
